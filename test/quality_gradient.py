# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# python3 -m test.quality_gradient

from brick.signal.signal_context import signal_context_t
from brick.marked_point.twoD.circle import circle_t
from brick.quality.gradient import bright_on_dark_gradient_t
from brick.signal.constant import INFINITY_NUMPY_FLOAT

import matplotlib.pyplot as pl_
import numpy as nmpy


image_size = (101, 101)  # Odd number preferably
max_hole_size = 0.14  # in [0,1]

signal_context_t.SetSignalsForQualityAndStatistics(image_size, None, None)

image_center = tuple(size // 2 for size in image_size)
radius = min(image_center) // 2
max_hole_radius = max(1, radius - 5)

circle = circle_t(image_center, radius)
c_map = circle.region
image = nmpy.zeros(image_size)
image[circle.bbox.domain][c_map] = 1

for half_band_height in range(0, max_hole_radius + 1, 2):
    image[
        (image_center[0] - half_band_height) : (image_center[0] + half_band_height + 1)
    ] = 0

    # /!\ signal_for_qty should normally not be written to directly
    signals = bright_on_dark_gradient_t.SignalsFromRawSignal(image, 2)
    signal_context_t.signal_for_qty = signals.signal_for_qty
    quality = bright_on_dark_gradient_t.MKPTQuality(
        circle, max_hole_size=max_hole_size
    )

    circle.quality = None  # Otherwise first, cached value is returned each time

    print(f"Hole size={2*half_band_height+1}, Quality={quality}")
    if quality == -INFINITY_NUMPY_FLOAT:
        pl_.matshow(image)
        pl_.show()
        break
