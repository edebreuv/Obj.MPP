# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# python3 -m test.region_map_w_dist_map

import brick.marked_point.mkpt_list as ml_
from brick.signal.signal_context import signal_context_t
from brick.marked_point.twoD.circle import circle_t

import matplotlib.pyplot as pl_


SIGNAL_LENGTHS = (200, 200)


signal_context_t.SetSignalsForQualityAndStatistics(SIGNAL_LENGTHS, None, None)

circles = [circle_t((100, 100), 20),
circle_t((120, 110), 30),
circle_t((60, 60), 15),
circle_t((180, 60), 15),
circle_t((50, 150), 16),
circle_t((62, 162), 16)]

region_map = ml_.RegionMapOfDetection(circles)
region_map[circles[3].bbox.domain] = circles[3].InnerOneDistanceMap()

pl_.matshow(region_map)
pl_.show()
