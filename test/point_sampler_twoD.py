# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# python3 -m test.sampler_twoD

from brick.marked_point.sampler import sampler_t
from brick.signal.signal_context import signal_context_t

import os.path as ph_  # Pathlib not necessary

import matplotlib.pyplot as pl_
import numpy as nmpy
import skimage.io as io_


def AccumulateUniSamples(samples, sampling) -> None:
    #
    rows = nmpy.rint(samples[0]).astype(int)
    cols = nmpy.rint(samples[1]).astype(int)
    for row, col in zip(rows, cols):
        sampling[row, col] += 1


def AccumulateMapOrPDFSamples(samples, sampling) -> None:
    #
    for row, col in zip(samples[0], samples[1]):
        sampling[row, col] += 1


def TestSampling() -> None:  # Avoids shadowing warnings
    #
    n_samples = 1_000_000
    pdf_filepath = ph_.join(ph_.dirname(__file__), "point_sampler_pdf.png")

    n_samples_as_str = "(" + "{:g}".format(n_samples) + " samples)"

    pdf = io_.imread(pdf_filepath)[:, :, 0]
    pmap = pdf > 0.75 * pdf.max()
    pl_.matshow(pmap), pl_.title("Sample generation constraint: Map")
    pl_.matshow(pdf / 255.0), pl_.title("Sample generation constraint: PDF")

    domain = (10, pdf.shape[0] - 1, 10, pdf.shape[1] - 1)
    domain_0_5 = (*domain, 0.5)
    domain_1_0 = (*domain, 1.0)
    domain_1_5 = (*domain, 1.5)
    domain_2_0 = (*domain, 2.0)
    domain_infty = domain

    signal_context_t.SetSignalsForQualityAndStatistics(pdf.shape, None, None)  # Fake signal context

    sampler = sampler_t.FromSeed(seed=0)
    sampling = nmpy.empty_like(pdf, dtype=nmpy.int64, order="C")

    whats = (domain_0_5, domain_1_0, domain_1_5, domain_2_0, domain_infty, pmap, pdf)
    hows = (
        AccumulateUniSamples,
        AccumulateUniSamples,
        AccumulateUniSamples,
        AccumulateUniSamples,
        AccumulateUniSamples,
        AccumulateMapOrPDFSamples,
        AccumulateMapOrPDFSamples,
    )
    titles = (
        "Uniform sampling 0.5",
        "Uniform sampling 1.0",
        "Uniform sampling 1.5",
        "Uniform sampling 2.0",
        "Uniform sampling Infty",
        "Map-based sampling",
        "PDF-based sampling",
    )
    for what, How, title in zip(whats, hows, titles,):
        sampler.SetPointParameters(what)
        samples = sampler["point"].NewAny(n_samples)

        sampling.fill(0)
        How(samples, sampling)
        if sampling.sum() != n_samples:
            raise RuntimeError("Incorrect number of generated samples")

        plt_step = 1
        if isinstance(what, tuple) and (what.__len__() == 5):
            precision = float(what[4])
            if precision.is_integer():
                plt_step = int(precision)
        pl_.matshow(sampling[::plt_step, ::plt_step])
        pl_.title(f"{title} {n_samples_as_str}\n"
                  f"{type(samples).__name__}"
                  f"[{type(samples[0]).__name__}"
                  f"[{type(samples[0][0]).__name__}]]")

    for f_idx in pl_.get_fignums():
        pl_.figure(f_idx)
        pl_.axis("off")

    pl_.show()


if __name__ == "__main__":
    #
    TestSampling()
