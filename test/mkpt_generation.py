# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# python3 -m test.mkpt_generation

import brick.structure.explorer as ex_
from brick.data.marked_point.twoD.std_marked_points import STD_MARKED_POINTS
from brick.signal.signal_context import signal_context_t

import itertools as it_

import matplotlib.pyplot as pl_
import numpy as nmpy


N_SAMPLES_PER_MARK_WO_RANGE = 4
N_SAMPLES_PER_MARK_W_RANGE = 7
SIGNAL_LENGTHS = (1000, 700)
ROW_MARGIN = 40
COL_MARGIN = 40
COL_TEXT_MARGIN = 20
COL_TEXT_WIDTH = 180


def TestMKPTGeneration():
    #
    vmap = nmpy.random.random(SIGNAL_LENGTHS) > 0.2
    signal_context_t.SetRawSignalAndValidityMap(None, vmap)
    signal_context_t.SetSignalsForQualityAndStatistics(SIGNAL_LENGTHS, None, None)
    fake_position = tuple(length // 2 for length in SIGNAL_LENGTHS)

    mkpt_classes = (
        mkpt_class
        for mkpt_class in ex_.StandardMarkedPoints("class")
        if mkpt_class.__name__[:-2] in STD_MARKED_POINTS
    )

    for mkpt_class in mkpt_classes:
        mkpt_name = mkpt_class.__name__[:-2]

        if (mkpt_class.dim > 2) or (mkpt_class.marks_details.__len__() > 3):
            continue

        mk_intervals = []
        for mk_name, mk_details in mkpt_class.marks_details.items():
            if mk_details.default_range is None:
                min_mk = mk_details.min
                max_mk = min(mk_details.max, 20)
            else:
                min_mk = mk_details.default_range[0]
                max_mk = mk_details.default_range[1]
            if not mk_details.min_inclusive:
                if mk_details.type is int:
                    min_mk += 1
                else:
                    min_mk = nmpy.nextafter(min_mk, min_mk + 1.0)
            if not mk_details.max_inclusive:
                if mk_details.type is int:
                    max_mk -= 1
                else:
                    max_mk = nmpy.nextafter(max_mk, max_mk - 1.0)

            if mk_details.default_range is None:
                mk_intervals.append(
                    nmpy.linspace(min_mk, max_mk, N_SAMPLES_PER_MARK_WO_RANGE)
                )
            else:
                mk_intervals.append(
                    nmpy.linspace(min_mk, max_mk, N_SAMPLES_PER_MARK_W_RANGE)
                )

        mkpt_map, text_to_be_plotted, row = InitialMapAndSuch()
        for marks in it_.product(*mk_intervals):
            mkpt = mkpt_class(fake_position, *marks)
            if not all(mkpt.region.shape):
                print(f"Empty {mkpt_name}: {row}:{marks}")
                continue
            if (nmpy.array(mkpt.region.shape) < 5).all():
                print(f"Tiny {mkpt_name}: {row}:{marks}")
                continue

            marks_as_str = []
            for mark in marks:
                if isinstance(mark, int) or mark.is_integer():
                    marks_as_str.append(mark.__str__())
                else:
                    marks_as_str.append(f"{mark:.2f}")

            regions = [mkpt.region]
            max_n_rows = mkpt.region.shape[0]
            for dilation in (-3, 3):
                region, _ = mkpt.Region(dilation)
                regions.append(region)
                max_n_rows = max(max_n_rows, region.shape[0])

            if row + max_n_rows > SIGNAL_LENGTHS[0]:
                PlotMKPTMap(mkpt_name, mkpt_map, text_to_be_plotted)
                mkpt_map, text_to_be_plotted, row = InitialMapAndSuch()

            text_to_be_plotted.append(
                (COL_TEXT_MARGIN, row + mkpt.region.shape[0] // 2 + 10, ",".join(marks_as_str))
            )

            col = COL_TEXT_MARGIN + COL_TEXT_WIDTH + COL_MARGIN
            for region in regions:
                cropped_map = mkpt_map[
                    row : (row + region.shape[0]), col : (col + region.shape[1])
                ]
                cropped_map[:, :] = 1
                cropped_map[region] = 2

                col += region.shape[1] + COL_MARGIN

            row += max_n_rows + ROW_MARGIN

        if mkpt_map.any():
            PlotMKPTMap(mkpt_name, mkpt_map, text_to_be_plotted)


def InitialMapAndSuch():
    #
    mkpt_map = nmpy.zeros(SIGNAL_LENGTHS, dtype=nmpy.uint8)
    text_to_be_plotted = []
    row = ROW_MARGIN

    return mkpt_map, text_to_be_plotted, row


def PlotMKPTMap(mkpt_name, mkpt_map, text_to_be_plotted):
    #
    pl_.matshow(mkpt_map)
    for text in text_to_be_plotted:
        pl_.text(*text, fontsize=7, color="w")

    figure = pl_.gcf()
    axes = pl_.gca()

    axes.set_axis_off()
    axes.set_title(mkpt_name)
    figure.set_size_inches(SIGNAL_LENGTHS[1] / figure.dpi, h=SIGNAL_LENGTHS[0] / figure.dpi)


if __name__ == "__main__":
    #
    TestMKPTGeneration()
    pl_.show()
