.. Obj.MPP documentation master file, created by
   sphinx-quickstart on Sun Feb  9 19:40:23 2020.

==============================================================
Obj.MPP: Object/pattern detection using a Marked Point Process
==============================================================

This documentation describes the usage of the Obj.MPP tools whose source code is available on a `GitLab repository <https://gitlab.inria.fr/edebreuv/Obj.MPP>`_.



What does Obj.MPP do?
=====================

In Images
---------

Looking for disk-like objects (*other shapes available*):

.. |image| image:: /graphics/obj-mpp-original.png
   :width: 100%

.. |i-caption| replace:: *Image with a region of interest (ROI) identified by the green rectangle.*

.. |result| image:: /graphics/obj-mpp-circle.png
   :width: 100%

.. |r-caption| replace:: *Disk-like objects found by Obj.MPP inside the ROI ("inside" in the sense "disk center is inside").*

.. _К562_(Light_microscopy): https://commons.wikimedia.org/wiki/File:%D0%9A562_(Light_microscopy).JPG
.. |source| replace:: Image source: `К562_(Light_microscopy)`_,
   Creative Commons Attribution-Share Alike 4.0 International
   (https://creativecommons.org/licenses/by-sa/4.0/deed.en)

+-------------+-------------+
| |image|     | |result|    |
+-------------+-------------+
| |i-caption| | |r-caption| |
+-------------+-------------+
| |source|                  |
+---------------------------+



In a Few Words
--------------

Obj.MPP implements the detection of *parametric objects* (or patterns) in a *signal* based on a *quality measure* using a Marked Point Process (MPP).

- Detecting an object in a signal amounts to finding a position *p* (a point) paired with a set of parameters *m* (called marks here [#]_), the quality measure of which being above some threshold.
- The detection task amounts to finding all such objects in the signal.

*Parametric object*
    An *n*-dimensional region defined by a finite set of parameters, typically real numbers. For example, a disk (*n*\ =2, position=center, mark(s)=radius). See :doc:`contents/users/object-types`.

*Signal*
    A mapping between an *n*-dimensional position and a *d*-dimensional vector. For example, a 2-dimensional grayscale image (*n*\ =2, *d*\ =1).

*Quality measure*
    A way to compute a real number measuring how well a given parametric object fits the signal at a given position. For example, object brightness compared to the surrounding area (i.e., object contrast). See :doc:`Standard Quality Measures <contents/users/quality-measures>`.



.. rubric:: Footnotes

.. [#] Hence the "marked point" terminology.



Bibliographic References
========================

`Detecting and quantifying stress granules in tissues of multicellular organisms with the Obj.MPP analysis tool <https://hal.archives-ouvertes.fr/hal-02193101v1>`_; de Graeve, Debreuve, Rahmoun, Ecsedi, Bahri, Hubstenberger, Descombes, Besse; Traffic (Wiley, ISSN: 1398-9219, ESSN: 1600-0854); 20(9), 697-711, 2019

DOI: `10.1111/tra.12678 <https://dx.doi.org/10.1111/tra.12678>`_

Citation: :download:`BibTeX <resource/obj-mpp-1.bib>` and :download:`EndNote <resource/obj-mpp-1.enw>`


`Multiple birth and cut algorithm for multiple object detection <https://hal.archives-ouvertes.fr/hal-00616371v1>`_; Gamal Eldin, Descombes, Charpiat, Zerubia; JMPT (ISSN: 0976-4127, ESSN: 0976-4135); 2012

DOI: 10.1.1.661.9041

Citation: :download:`BibTeX <resource/obj-mpp-2.bib>` and :download:`EndNote <resource/obj-mpp-2.enw>`



Installation
============

Follow the instructions on the :doc:`Installation and Requirements <contents/installation>` page.



Documentation Sections
======================

- :doc:`For Users <contents/users/index>` = *Software users* willing to:

    - detect objects...

        - using ``mpp_detector_cli.py`` (*command line interface*)
        - or ``mpp_detector_gi.py`` (*graphical interface*)
        - and the provided object type and object quality definitions

- :doc:`For Developers <contents/developers/index>` = *Library users* willing to:

    - add the Obj.MPP object detection capability in some piece of software...

        - using the core module ``mpp.py``
        - and the provided object type and object quality definitions

- :doc:`For Customizers <contents/customizers/index>` = *Library hackers* willing to:

    - modify ``mpp.py``
    - add new object type and object quality definitions

- :doc:`For Reviewers <contents/reviewers/index>` = *Code checkers* willing to:

    - understand the structure of Obj.MPP
    - hunt for bugs
    - imagine structure, code readability, code conventions, code conformity, performance improvements

Typical browsing route for users:

1. :doc:`Obj.MPP for Users <contents/users/index>`
2. :doc:`contents/users/mpp-detector-cli`
3. :doc:`contents/users/configuration-file`
4. :doc:`contents/users/generic-configurations`
5. :doc:`contents/users/object-types`
6. :doc:`Standard Quality Measures <contents/users/quality-measures>`.



Sitemap
=======

.. toctree::
   :maxdepth: 2

   Introduction    <self>
   Getting Obj.MPP <contents/installation.rst>
   For Users       <contents/users/index.rst>
   For Developers  <contents/developers/index.rst>
   For Customizers <contents/customizers/index.rst>
   For Reviewers   <contents/reviewers/index.rst>
   contents/users/mpp-detector-cli.rst
   contents/users/mpp-detector-cli-bat.rst
   contents/users/mpp-detector-gi.rst
   contents/users/mpp-quality-chooser.rst
   contents/users/configuration-file.rst
   contents/users/generic-configurations.rst
   contents/users/object-types.rst
   Standard Quality Measures <contents/users/quality-measures.rst>
