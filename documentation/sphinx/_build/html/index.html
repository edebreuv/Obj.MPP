
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Obj.MPP: Object/pattern detection using a Marked Point Process &#8212; Obj.MPP  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/sphinxdoc.css" />
    <link rel="stylesheet" href="_static/css/custom.css" type="text/css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Obj.MPP Installation" href="contents/installation.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="contents/installation.html" title="Obj.MPP Installation"
             accesskey="N">next</a> |</li>
        <li class="nav-item nav-item-0"><a href="#">Obj.MPP  documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Obj.MPP: Object/pattern detection using a Marked Point Process</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="obj-mpp-object-pattern-detection-using-a-marked-point-process">
<h1>Obj.MPP: Object/pattern detection using a Marked Point Process<a class="headerlink" href="#obj-mpp-object-pattern-detection-using-a-marked-point-process" title="Permalink to this headline">¶</a></h1>
<p>This documentation describes the usage of the Obj.MPP tools whose source code is available on a <a class="reference external" href="https://gitlab.inria.fr/edebreuv/Obj.MPP">GitLab repository</a>.</p>
<section id="what-does-obj-mpp-do">
<h2>What does Obj.MPP do?<a class="headerlink" href="#what-does-obj-mpp-do" title="Permalink to this headline">¶</a></h2>
<section id="in-images">
<h3>In Images<a class="headerlink" href="#in-images" title="Permalink to this headline">¶</a></h3>
<p>Looking for disk-like objects (<em>other shapes available</em>):</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="row-odd"><td><p><a class="reference internal" href="_images/obj-mpp-original.png"><img alt="image" src="_images/obj-mpp-original.png" style="width: 100%;" /></a></p></td>
<td><p><a class="reference internal" href="_images/obj-mpp-circle.png"><img alt="result" src="_images/obj-mpp-circle.png" style="width: 100%;" /></a></p></td>
</tr>
<tr class="row-even"><td><p><em>Image with a region of interest (ROI) identified by the green rectangle.</em></p></td>
<td><p><em>Disk-like objects found by Obj.MPP inside the ROI (“inside” in the sense “disk center is inside”).</em></p></td>
</tr>
<tr class="row-odd"><td colspan="2"><p>Image source: <a class="reference external" href="https://commons.wikimedia.org/wiki/File:%D0%9A562_(Light_microscopy).JPG">К562_(Light_microscopy)</a>,
Creative Commons Attribution-Share Alike 4.0 International
(<a class="reference external" href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">https://creativecommons.org/licenses/by-sa/4.0/deed.en</a>)</p></td>
</tr>
</tbody>
</table>
</section>
<section id="in-a-few-words">
<h3>In a Few Words<a class="headerlink" href="#in-a-few-words" title="Permalink to this headline">¶</a></h3>
<p>Obj.MPP implements the detection of <em>parametric objects</em> (or patterns) in a <em>signal</em> based on a <em>quality measure</em> using a Marked Point Process (MPP).</p>
<ul class="simple">
<li><p>Detecting an object in a signal amounts to finding a position <em>p</em> (a point) paired with a set of parameters <em>m</em> (called marks here <a class="footnote-reference brackets" href="#id2" id="id1">1</a>), the quality measure of which being above some threshold.</p></li>
<li><p>The detection task amounts to finding all such objects in the signal.</p></li>
</ul>
<dl class="simple">
<dt><em>Parametric object</em></dt><dd><p>An <em>n</em>-dimensional region defined by a finite set of parameters, typically real numbers. For example, a disk (<em>n</em>=2, position=center, mark(s)=radius). See <a class="reference internal" href="contents/users/object-types.html"><span class="doc">Standard Object Types</span></a>.</p>
</dd>
<dt><em>Signal</em></dt><dd><p>A mapping between an <em>n</em>-dimensional position and a <em>d</em>-dimensional vector. For example, a 2-dimensional grayscale image (<em>n</em>=2, <em>d</em>=1).</p>
</dd>
<dt><em>Quality measure</em></dt><dd><p>A way to compute a real number measuring how well a given parametric object fits the signal at a given position. For example, object brightness compared to the surrounding area (i.e., object contrast). See <a class="reference internal" href="contents/users/quality-measures.html"><span class="doc">Standard Quality Measures</span></a>.</p>
</dd>
</dl>
<p class="rubric">Footnotes</p>
<dl class="footnote brackets">
<dt class="label" id="id2"><span class="brackets"><a class="fn-backref" href="#id1">1</a></span></dt>
<dd><p>Hence the “marked point” terminology.</p>
</dd>
</dl>
</section>
</section>
<section id="bibliographic-references">
<h2>Bibliographic References<a class="headerlink" href="#bibliographic-references" title="Permalink to this headline">¶</a></h2>
<p><a class="reference external" href="https://hal.archives-ouvertes.fr/hal-02193101v1">Detecting and quantifying stress granules in tissues of multicellular organisms with the Obj.MPP analysis tool</a>; de Graeve, Debreuve, Rahmoun, Ecsedi, Bahri, Hubstenberger, Descombes, Besse; Traffic (Wiley, ISSN: 1398-9219, ESSN: 1600-0854); 20(9), 697-711, 2019</p>
<p>DOI: <a class="reference external" href="https://dx.doi.org/10.1111/tra.12678">10.1111/tra.12678</a></p>
<p>Citation: <a class="reference download internal" download="" href="_downloads/ffb58960eba15a1993e05f9eefddc41f/obj-mpp-1.bib"><code class="xref download docutils literal notranslate"><span class="pre">BibTeX</span></code></a> and <a class="reference download internal" download="" href="_downloads/6a6055e8d455653cea82913131c01d2e/obj-mpp-1.enw"><code class="xref download docutils literal notranslate"><span class="pre">EndNote</span></code></a></p>
<p><a class="reference external" href="https://hal.archives-ouvertes.fr/hal-00616371v1">Multiple birth and cut algorithm for multiple object detection</a>; Gamal Eldin, Descombes, Charpiat, Zerubia; JMPT (ISSN: 0976-4127, ESSN: 0976-4135); 2012</p>
<p>DOI: 10.1.1.661.9041</p>
<p>Citation: <a class="reference download internal" download="" href="_downloads/3a5b9ec00e1974ec107c9e92c6b3b76d/obj-mpp-2.bib"><code class="xref download docutils literal notranslate"><span class="pre">BibTeX</span></code></a> and <a class="reference download internal" download="" href="_downloads/1bb9c2c99f4a3e42b62a06c4dd5e5e48/obj-mpp-2.enw"><code class="xref download docutils literal notranslate"><span class="pre">EndNote</span></code></a></p>
</section>
<section id="installation">
<h2>Installation<a class="headerlink" href="#installation" title="Permalink to this headline">¶</a></h2>
<p>Follow the instructions on the <a class="reference internal" href="contents/installation.html"><span class="doc">Installation and Requirements</span></a> page.</p>
</section>
<section id="documentation-sections">
<h2>Documentation Sections<a class="headerlink" href="#documentation-sections" title="Permalink to this headline">¶</a></h2>
<ul>
<li><p><a class="reference internal" href="contents/users/index.html"><span class="doc">For Users</span></a> = <em>Software users</em> willing to:</p>
<blockquote>
<div><ul>
<li><p>detect objects…</p>
<blockquote>
<div><ul class="simple">
<li><p>using <code class="docutils literal notranslate"><span class="pre">mpp_detector_cli.py</span></code> (<em>command line interface</em>)</p></li>
<li><p>or <code class="docutils literal notranslate"><span class="pre">mpp_detector_gi.py</span></code> (<em>graphical interface</em>)</p></li>
<li><p>and the provided object type and object quality definitions</p></li>
</ul>
</div></blockquote>
</li>
</ul>
</div></blockquote>
</li>
<li><p><a class="reference internal" href="contents/developers/index.html"><span class="doc">For Developers</span></a> = <em>Library users</em> willing to:</p>
<blockquote>
<div><ul>
<li><p>add the Obj.MPP object detection capability in some piece of software…</p>
<blockquote>
<div><ul class="simple">
<li><p>using the core module <code class="docutils literal notranslate"><span class="pre">mpp.py</span></code></p></li>
<li><p>and the provided object type and object quality definitions</p></li>
</ul>
</div></blockquote>
</li>
</ul>
</div></blockquote>
</li>
<li><p><a class="reference internal" href="contents/customizers/index.html"><span class="doc">For Customizers</span></a> = <em>Library hackers</em> willing to:</p>
<blockquote>
<div><ul class="simple">
<li><p>modify <code class="docutils literal notranslate"><span class="pre">mpp.py</span></code></p></li>
<li><p>add new object type and object quality definitions</p></li>
</ul>
</div></blockquote>
</li>
<li><p><a class="reference internal" href="contents/reviewers/index.html"><span class="doc">For Reviewers</span></a> = <em>Code checkers</em> willing to:</p>
<blockquote>
<div><ul class="simple">
<li><p>understand the structure of Obj.MPP</p></li>
<li><p>hunt for bugs</p></li>
<li><p>imagine structure, code readability, code conventions, code conformity, performance improvements</p></li>
</ul>
</div></blockquote>
</li>
</ul>
<p>Typical browsing route for users:</p>
<ol class="arabic simple">
<li><p><a class="reference internal" href="contents/users/index.html"><span class="doc">Obj.MPP for Users</span></a></p></li>
<li><p><a class="reference internal" href="contents/users/mpp-detector-cli.html"><span class="doc">Using mpp_detector_cli.py</span></a></p></li>
<li><p><a class="reference internal" href="contents/users/configuration-file.html"><span class="doc">Configuration File</span></a></p></li>
<li><p><a class="reference internal" href="contents/users/generic-configurations.html"><span class="doc">Generic Configurations</span></a></p></li>
<li><p><a class="reference internal" href="contents/users/object-types.html"><span class="doc">Standard Object Types</span></a></p></li>
<li><p><a class="reference internal" href="contents/users/quality-measures.html"><span class="doc">Standard Quality Measures</span></a>.</p></li>
</ol>
</section>
<section id="sitemap">
<h2>Sitemap<a class="headerlink" href="#sitemap" title="Permalink to this headline">¶</a></h2>
<div class="toctree-wrapper compound">
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/installation.html">Getting Obj.MPP</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/installation.html#installation">Installation</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/installation.html#requirements">Requirements</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/index.html">For Users</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/users/index.html#usage-os-independent">Usage: OS-independent</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/developers/index.html">For Developers</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/customizers/index.html">For Customizers</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/reviewers/index.html">For Reviewers</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/reviewers/index.html#signal-loading-diagram">Signal Loading Diagram</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-cli.html">Using mpp_detector_cli.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-cli-bat.html">Using mpp_detector_cli.bat</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-gi.html">Using mpp_detector_gi.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-quality-chooser.html">Using mpp_quality_chooser.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/configuration-file.html">Configuration File</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/users/configuration-file.html#example-configuration">Example Configuration</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/configuration-file.html#generic-configurations">Generic Configurations</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/generic-configurations.html">Generic Configurations</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/users/generic-configurations.html#minimal">Minimal</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/generic-configurations.html#basic">Basic</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/generic-configurations.html#full">Full</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/object-types.html">Standard Object Types</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/users/object-types.html#specifying-an-object-type">Specifying an Object Type</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/object-types.html#dimensional-objects">2-Dimensional Objects</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/object-types.html#id1">3-Dimensional Objects</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/object-types.html#marks-details">Marks Details</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/quality-measures.html">Standard Quality Measures</a><ul>
<li class="toctree-l2"><a class="reference internal" href="contents/users/quality-measures.html#quality-measures">Quality Measures</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/quality-measures.html#parameters-of-quality-measures">Parameters of Quality Measures</a></li>
<li class="toctree-l2"><a class="reference internal" href="contents/users/quality-measures.html#signal-transformations">Signal Transformations</a></li>
</ul>
</li>
</ul>
</div>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="#">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Obj.MPP: Object/pattern detection using a Marked Point Process</a><ul>
<li><a class="reference internal" href="#what-does-obj-mpp-do">What does Obj.MPP do?</a><ul>
<li><a class="reference internal" href="#in-images">In Images</a></li>
<li><a class="reference internal" href="#in-a-few-words">In a Few Words</a></li>
</ul>
</li>
<li><a class="reference internal" href="#bibliographic-references">Bibliographic References</a></li>
<li><a class="reference internal" href="#installation">Installation</a></li>
<li><a class="reference internal" href="#documentation-sections">Documentation Sections</a></li>
<li><a class="reference internal" href="#sitemap">Sitemap</a></li>
</ul>
</li>
</ul>
<h3><a href="#">Sitemap</a></h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Introduction</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/installation.html">Getting Obj.MPP</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/index.html">For Users</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/developers/index.html">For Developers</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/customizers/index.html">For Customizers</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/reviewers/index.html">For Reviewers</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-cli.html">Using mpp_detector_cli.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-cli-bat.html">Using mpp_detector_cli.bat</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-detector-gi.html">Using mpp_detector_gi.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/mpp-quality-chooser.html">Using mpp_quality_chooser.py</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/configuration-file.html">Configuration File</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/generic-configurations.html">Generic Configurations</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/object-types.html">Standard Object Types</a></li>
<li class="toctree-l1"><a class="reference internal" href="contents/users/quality-measures.html">Standard Quality Measures</a></li>
</ul>

<a href= "genindex.html">Index</a>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="contents/installation.html" title="Obj.MPP Installation"
             >next</a> |</li>
        <li class="nav-item nav-item-0"><a href="#">Obj.MPP  documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Obj.MPP: Object/pattern detection using a Marked Point Process</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, Eric Debreuve.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.2.0.
    </div>
  </body>
</html>