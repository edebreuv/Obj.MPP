

%0 Journal Article
%T Multiple Birth and Cut Algorithm for Multiple Object Detection
%+ Inverse problems in earth monitoring (ARIANA)
%+ Perception Understanding Learning Systems for Activity Recognition (PULSAR)
%A Gamal Eldin, Ahmed
%A Descombes, Xavier
%A Charpiat, Guillaume
%A Zerubia, Josiane
%< avec comité de lecture
%@ 0976-4127
%J Journal of Multimedia Processing and Technologies
%I Digital Information Research Foundation
%8 2012
%D 2012
%K flamingo counting
%K optimization
%K graph cut
%K belief propagation
%K Point process
%K multiple birth and cut
%Z Statistics [stat]/Machine Learning [stat.ML]
%Z Computer Science [cs]/Image Processing [eess.IV]
%Z Computer Science [cs]/Computer Vision and Pattern Recognition [cs.CV]Journal articles
%X In this paper, we describe a new optimization method which we call Multiple Birth and Cut (MBC). It combines the recently developed Multiple Birth and Death (MBD) algorithm and the Graph-Cut algorithm. MBD and MBC optimization methods are applied to energy minimization of an object based model, the marked point process. We compare the MBC to the MBD showing their respective advantages and drawbacks, where the most important advantage of the MBC is the reduction of number of parameters. We demonstrate that by proposing good candidates throughout the selection phase in the birth step, the speed of convergence is increased. In this selection phase, the best candidates are chosen from object sets by a belief propagation algorithm. We validate our algorithm on the flamingo counting problem in a colony and demonstrate that our algorithm outperforms the MBD algorithm.
%G English
%Z Ariana/INRIA
%Z Pulsar/INRIA
%2 https://hal.archives-ouvertes.fr/hal-00616371/document
%2 https://hal.archives-ouvertes.fr/hal-00616371/file/paper.pdf
%L hal-00616371
%U https://hal.archives-ouvertes.fr/hal-00616371
%~ UNICE
%~ CNRS
%~ INRIA
%~ INRIA-SOPHIA
%~ I3S
%~ INRIASO
%~ INRIA_TEST
%~ UCA-TEST
%~ UNIV-COTEDAZUR