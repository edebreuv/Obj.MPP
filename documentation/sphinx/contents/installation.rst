====================
Obj.MPP Installation
====================

Installation
============

Using Tools
-----------

The ``Obj.MPP`` project is published on the `Python Package Index (PyPI) <https://pypi.org>`_ at: `https://pypi.org/project/obj.mpp <https://pypi.org/project/obj.mpp>`_. It requires version 3.8, or newer, of the interpreter. It should be installable from Python distribution platforms or Integrated Development Environments (IDEs). Otherwise, it can be installed from a command-line console:

- For all users, after acquiring administrative rights:
    - First installation: ``pip3 install obj.mpp``
    - Installation update: ``pip3 install --upgrade obj.mpp``
- For the current user (no administrative rights required):
    - First installation: ``pip3 install --user obj.mpp``
    - Installation update: ``pip3 install --user --upgrade obj.mpp``


.. note::
    The command ``pip3`` was mentioned above to emphasize that ``Obj.MPP`` requires major version 3 of Python. If ``pip`` defaults to this version, it can of course be used instead.



Manually
--------

To *install* ``Obj.MPP`` manually anywhere in a non-system location, download the project available in the `source code repository <https://gitlab.inria.fr/edebreuv/Obj.MPP>`_, for example as a `ZIP archive <https://gitlab.inria.fr/edebreuv/Obj.MPP/-/archive/master/Obj.MPP-master.zip>`_. Check that all the requirements are met (see Section :ref:`Requirements<sec-requirements>`). Then follow the instructions :doc:`users/index`.



.. _sec-requirements:

Requirements
============

.. index:: PowerShell, Windows

The standalone distribution of ``mpp_detector_cli.py`` for Windows (see :ref:`Windows section<sec-windows>`) requires **Windows PowerShell**. Recent versions of Windows normally come with PowerShell pre-installed. If not, instructions to install it can easily be found.

.. index:: Linux, MacOS

To use the Obj.MPP tools in an OS-independent way (see :ref:`OS-independent section<sec-os-indep>`), the requirements are:

- the **latest Python language interpreter of major version 3** (see note on :ref:`Python version<not-python>`)
- the following Python packages (the mentioned versions are the ones used for development)

    - pillow
    - PyQt5
    - colorama
    - imageio
    - matplotlib
    - networkx
    - numpy
    - scikit_image
    - scipy

- a **console (a.k.a. terminal) application** on Linux or MacOS (console application also usable on Windows, but not required)

.. _not-python:

.. note:: For sure, pre-3.6 Python versions are not recent enough. The Obj.MPP tools might work with pre-latest versions though. However, the source code is regularly "upgraded" to benefit from new features of the language.
