=======================
Obj.MPP for Customizers
=======================

This section targets customizers, that is, *library hackers* willing to:

- modify ``mpp.py``
- add new object type and object quality definitions
