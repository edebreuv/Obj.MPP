=====================
Obj.MPP for Reviewers
=====================

This section targets reviewers, that is, *code checkers* willing to:

- understand the structure of Obj.MPP
- hunt for bugs
- imagine structure, code readability, code conventions, code conformity, performance improvements



Signal Loading Diagram
======================

The following diagram presents how signals are loaded and processed in ``mpp_detector_cli.py``.

.. image:: /graphics/signal-loading.svg
   :align: center
