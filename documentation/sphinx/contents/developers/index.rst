======================
Obj.MPP for Developers
======================

This section targets developers, that is, *library clients* willing to:

- add the Obj.MPP object detection capability in some piece of software...

    - using the core module ``mpp.py``
    - and the provided object type and object quality definitions
