============================
Using mpp_quality_chooser.py
============================

The main steps of the graphical interface usage for

- quality measure selection and
- tuning of object mark ranges

are identified in the screenshot below.

.. image:: /graphics/mpp-qty-chooser-annotated.png
   :align: center
