======================
Generic Configurations
======================

The values set for mandatory parameters are just examples. They should be adapted to the problem at hand. The values set for the other parameters are their default values.

Sections that are empty in the minimal and basic configurations must really be adapted to the problem at hand since the default values of their parameters are surely inappropriate. Additional empty sections in the full configuration are dedicated to optional parameters of the chosen quality measure, its corresponding signal transformation, and the way the detection result is reported.



Minimal
=======

.. literalinclude:: /resource/minimal.ini
   :language: INI



Basic
=====

.. literalinclude:: /resource/basic.ini
   :language: INI



Full
====

.. literalinclude:: /resource/full.ini
   :language: INI
