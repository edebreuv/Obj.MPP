=====================
Standard Object Types
=====================

Specifying an Object Type
=========================

The desired object type is specified in the ``[object]`` section of a configuration file using the ``object_type`` parameter, for example ``object_type = circle`` (see :doc:`configuration-file`).



2-Dimensional Objects
=====================

Current standard object types are:

.. include:: /resource/object-types-2d.rst

.. image:: /graphics/object-types-2d.png
   :width: 50%

See Section :ref:`mark-details` for details about the respective marks of the object types.



3-Dimensional Objects
=====================

Current standard object types are:

.. include:: /resource/object-types-3d.rst

.. image:: /graphics/object-types-3d.png
   :width: 30%

See Section :ref:`mark-details` for details about the respective marks of the object types.



.. _mark-details:

Marks Details
=============

.. note:: The mark ranges in a configuration file are named with the name of the mark followed by the suffix "_rng", for example ``radius_rng`` for the mark ``radius``.

The desired mark ranges are specified in the ``[object ranges]`` section of a configuration file (see :doc:`configuration-file`). To specify a mark range, use one of the following statements

- ``MARK_NAME_rng = (min_desired_value, max_desired_value)``
- ``MARK_NAME_rng = (min_desired_value, max_desired_value, value_precision)``
- Examples: ``radius_rng = (0, 10)`` and ``radius_rng = (0, 10, 2)``

If no precision is given, then the detection algorithm will generate random mark values uniformly in the given interval at machine precision. Otherwise, the random values will be uniformly drawn among the values in the given interval that can be written ``min_desired_value + k * value_precision`` where ``k`` is an integer.

The following table describes the marks of the standard object types. Some values might be rounded. Yet, one can identify typical multiples or fractions of :math:`\pi`. Very large or tiny numbers will often correspond to limit values of the floating point representation used in the code.

.. include:: /resource/object-types-array.rst
