===============================================================
Standard Quality Measures and Associated Signal Transformations
===============================================================

Quality Measures
================

The desired quality measure is specified in the ``[quality]`` section of a configuration file using the ``object_quality`` parameter, for example ``object_quality = bright_on_dark_contrast``. The minimum quality value an object must have to be considered acceptable is specified using the ``min_quality`` parameter in the same section, for example ``min_quality = 1`` (see :doc:`configuration-file`).

Current standard quality measures are:

.. include:: /resource/quality-measures.rst

.. note:: Although quality measures might be described as "Python functions" in the documentation, they are in fact Python classes. The full name of these classes are obtained by suffixing the names used in the documentation by "_t". For example, ``bright_on_dark_contrast_t`` for ``bright_on_dark_contrast``.

**The** ``contour`` **quality** aggregates the signal along the contour of the object. The supported aggregation functions are: ``mean``, ``stddev`` (standard deviation), ``variance``, ``median``, ``min``, and ``max``. The associated parameter is named ``measure``. Its default value is ``mean``.

**The contrast-based qualities** measure how far apart the average signal value inside the object and the average in its neighborhood are.

**The gradient-based qualities** measure how aligned the gradient of the signal along the object contour and the contour normal are. The parameters of these quality measures are:

- ``bright_on_dark_contrast``

    - ``ring_thickness_ext``: thickness of the outside ring used to compute object contrast.
    - ``ring_thickness_int``: thickness of the inside ring used to compute object contrast. By default, use full object interior.
    - ``normalized``: set to True or False to choose whether to normalize the contrast by the square root of the product of the standard deviations in the outside and inside rings.

- ``bright_on_dark_gradient``

    - ``max_hole_size``: gradient and normal can be consecutively misaligned for at most this length.

- The dark-on-bright counterparts have the same parameters.

The desired quality parameters are specified in the optional ``[quality prm]`` section of a configuration file, for example ``ring_thickness_ext = 3`` (see :doc:`configuration-file`). However, note that all these parameters have default values (which explains why the ``[quality prm]`` section is optional).

For details about the respective parameters of the quality measures, see Section :ref:`quality-measures-prms`



.. _quality-measures-prms:

Parameters of Quality Measures
==============================

The following table describes the parameters of the standard quality measures. Some values might be rounded. The value ``inf`` represents a value larger than any other one.

.. include:: /resource/quality-measures-array.rst



Signal Transformations
======================

The signal transformation associated with a quality measure is never explicit in a configuration file since it is hardcoded. For the sake of information, the transformation-quality associations are:

- Contour quality measure: the signal is used as is.
- Contrast-based quality measures: the signal is used as is.
- Gradient-based quality measures: the transformation computes the gradient of the signal. It has an optional parameters: ``unitary``. If True, the signal gradient will be normalized. In other words, the quality measures will not depend on the gradient norm, but only its direction.
