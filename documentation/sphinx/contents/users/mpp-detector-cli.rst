=========================
Using mpp_detector_cli.py
=========================

The command ``mpp_detector_cli.py`` takes a configuration file as argument. All configuration parameters can also be overwritten by adding arguments in the form ``--section-parameter value``, for example ``--mpp-n_iterations 1000`` to set the parameter ``n_iterations`` in section ``mpp`` to 1000. Note that any space in the section name must be replaced with the underscore character, for example ``object_ranges`` for section ``object ranges``. For details about the configuration file, please refer to

- :doc:`configuration-file`
- :doc:`generic-configurations`
- :doc:`object-types`
- :doc:`Standard Quality Measures <quality-measures>`
