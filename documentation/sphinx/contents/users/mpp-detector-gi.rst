========================
Using mpp_detector_gi.py
========================

.. note:: The GRAPHICAL INTERFACE VERSION of Obj.MPP is CURRENTLY UNDERGOING an OVERHAUL. It is PREFERABLE to TURN to the COMMAND-LINE VERSION.

The main steps of the graphical interface usage for object detection are identified in the screenshot below.

.. image:: /graphics/mpp-detector-gi-annotated.png
   :align: center
