=================
Obj.MPP for Users
=================

.. _sec-os-indep:

Usage: OS-independent
=====================

General Instructions
--------------------

The steps to use the Obj.MPP tools are:

- **download** the project available in the `source code repository <https://gitlab.inria.fr/edebreuv/Obj.MPP>`_, for example as a `ZIP archive <https://gitlab.inria.fr/edebreuv/Obj.MPP/-/archive/master/Obj.MPP-master.zip>`_
- **decompress** the archive
- **open** a console (a terminal application on Linux or MacOS, or the command prompt on Windows [#]_)
- **move** to the folder containing ``mpp_detector_cli.py`` [#]_
- **run** one of the following commands (see :ref:`Detailed usage<sec-detailed-usage>`):

    - ``python3 mpp_detector_cli.py path_to_an_INI_configuration_file``
    - ``python3 mpp_detector_gi.py``
    - ``python3 mpp_quality_chooser.py``

.. note:: The use of ``python3`` as the Python interpreter command is a way to emphasize that the Obj.MPP tools must be interpreted using version 3 of the Python language. Actual interpreter command name might be different depending on OS configuration.

The script ``mpp_detector_cli.py`` is the command-line version of Obj.MPP. It allows "more advanced" users to write their own INI configuration file [#]_, and then launch the detection algorithm with this INI file as input.

The script ``mpp_detector_gi.py`` is the graphical version of Obj.MPP and exposes only a subset of the configuration parameters that ``mpp_detector_cli.py`` accepts. It is more targeted toward regular users.

The script ``mpp_quality_chooser.py`` is a graphical tool allowing to infer good configuration parameters regarding object parameters and object quality. It is potentially useful in either case.



.. _sec-detailed-usage:

Detailed Usage
--------------

- :doc:`mpp-detector-cli`
- :doc:`mpp-detector-gi`
- :doc:`mpp-quality-chooser`



.. rubric:: Footnotes

.. [#] Other options might be available, like Cygwin on Windows.
.. [#] .. image:: /graphics/folder-obj-mpp.png
.. [#] E.g., by getting inspiration from ``resource/twoD/microscopy-circle.ini`` or by checking the full specification in ``brick/data/config/specification.py``.



.. _sec-windows:

.. Usage: Windows Only
.. ===================
