==================
Configuration File
==================

Example Configuration
=====================

Configuration files follow the INI format. Sections are introduced by ``[section name]``. In a section, parameters are defined by ``parameter_name = value``. Comments start with the hash character # and end with the end of the line. Below are examples of the main sections.

General detection section:

.. code-block:: ini

    [mpp]
    n_iterations            = 1000 # Number of detection iterations
    n_births_per_iteration  = 50   # Number of object proposals per iteration

Section about constraints on detected objects. Currently, only one constraint is implemented: non-overlapping between any 2 objects, with some tolerance:

.. code-block:: ini

    [constraints]
    overlap_tolerance       = 20.0 # Accepts up to 20% in overlapping between 2 objects

Detection refinement section (can be safely removed):

.. code-block:: ini

    [refinement]
    age_for_refinement      = 40
    n_refinement_attempts   = 40
    refinement_fraction     = 0.2

When an object has not been replaced by a better overlapping one for ``age_for_refinement`` number of iterations, ``n_refinement_attempts`` slight variations of it are generated (how "slight" the variations are is defined by ``refinement_fraction``), the best of which replaces the object if better.

Execution feedback:

.. code-block:: ini

    [feedback]
    status_period           = 2 # Period of reporting of current detection state

Sections about the *a priori* on the objects of interest:

.. code-block:: ini

    [object]
    object_type             = circle # Idealized shape of objects of interest

    # Section of the ranges of the object mark(s)
    [object ranges]
    radius_rng              = (20, 35, 0.5) # Range of the mark "radius": min value, max value, and precision

Section about the object quality measure:

.. code-block:: ini

    [quality]
    object_quality          = dark_on_bright_gradient # Python "function" to compute the quality measure
    min_quality             = 0.75                    # Minimal quality measure of detected objects

Section about the signal to look for objects in:

.. code-block:: ini

    [signal]
    signal_loading_function = SingleChannelOfImage # Python function able to load the signal file
    signal_path             = microscopy.png       # Name of signal file

    # Section of the parameters of the signal loading function (if any)
    [signal loading prm]
    channel                 = gray

    # Section of the parameter(s) of the signal transformation function
    [signal processing prm]
    unitary                 = False

The quality measure computation might operate on a transformation of the loaded signal. This transformation might take tunable parameters, which are then specified in the ``signal processing prm`` section. Actually, to each quality measure corresponds a signal transformation, be it the no-op.

Section about how the detected objects are presented and/or stored:

.. code-block:: ini

    [output]
    console                 = True

    result_output_function  = OutputObjectsOnSingleChannelBackground



Generic Configurations
======================

More details about available sections and parameters: :doc:`generic-configurations`.
