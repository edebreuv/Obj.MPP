#!/usr/bin/env sh

PROJECT_FOLDER="$HOME/Code/app/mno/obj.mpp"
DOC_PDOC3_FOLDER="$PROJECT_FOLDER/documentation/api/pdoc3"
DOC_PYDOCTOR_FOLDER="$PROJECT_FOLDER/documentation/api/pydoctor"

mkdir -p "$DOC_PDOC3_FOLDER"
mkdir -p "$DOC_PYDOCTOR_FOLDER"

MODULES_FILE=`mktemp`

git ls-files .. | \
grep '\.py$' | \
sed -n -e 's/^\.\.\///p' | \
sed -n -e '/^brick\//p' -e '/^[^\/]*$/p' | \
grep -v '^setup' | \
while read module
do
    echo "$PROJECT_FOLDER/$module" >> "$MODULES_FILE"
done

PDOC3_MODULES=
PYDOCTOR_MODULES=
for module in `cat "$MODULES_FILE"`
do
    PDOC3_MODULES="$PDOC3_MODULES $module"
    PYDOCTOR_MODULES="$PYDOCTOR_MODULES --add-package=$module"
done

# pdoc3 --html --force --output-dir "$DOC_PDOC3_FOLDER" $PDOC3_MODULES
pydoctor --project-name=Obj.MPP \
    --project-url='https://gitlab.inria.fr/edebreuv/Obj.MPP' \
    --project-base-dir="$PROJECT_FOLDER" \
    $PYDOCTOR_MODULES \
    --html-output="$DOC_PYDOCTOR_FOLDER"
