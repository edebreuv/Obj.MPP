#!/bin/sh

# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

if [ $# -gt 0 ]
then
    [ "$1" = -n ] \
        && dry_run="true" \
        || { echo "$1"': Invalid option'; exit 1; }
else
    dry_run=
fi

# See sys.platform
OS_LINUX=linux
OS_WINDOWS=win32

CC_LIN=gcc
CC_WIN=x86_64-w64-mingw32-gcc

CFLAGS='-Wall -O2'
#        -fopenmp'

CFLAGS_LIN=-fpic
CFLAGS_WIN=

LFLAGS='-Wall -O2
        -shared
        -lm'
#        -fopenmp'

LFLAGS_LIN=
LFLAGS_WIN='-static -static-libgcc -static-libgfortran
            -lopenblas -llapack -lgfortran -lquadmath'

if [ -z "$dry_run" ]
then
CompileExtension() {
    ext_path="$1"
    OS_TYPE="$2"
    CC="$3"
    CFLAGS_OS="$4"
    LFLAGS_OS="$5"

    ext_basename=`basename "$ext_path" .c`
    folder=`dirname "$ext_path"`
    path_wo_os_ext="$folder/$ext_basename"
    path_wo_ext="$path_wo_os_ext-$OS_TYPE"

    echo "--- Building $ext_basename for $OS_TYPE..."
    $CC $CFLAGS_OS $CFLAGS -o "$path_wo_os_ext".o -c "$ext_path"
    $CC $LFLAGS_OS $LFLAGS -o "$path_wo_ext".so "$path_wo_os_ext".o
    strip         --strip-all "$path_wo_ext".so
    rm "$path_wo_os_ext".o
}
else
CompileExtension() {
    ext_path="$1"
    OS_TYPE="$2"
    CC="$3"
    CFLAGS_OS=`echo "$4" | tr '\n' ' ' | tr -s ' '`
    LFLAGS_OS=`echo "$5" | tr '\n' ' ' | tr -s ' '`

    echo 'Extension: '"$ext_path"
    echo 'OS:        '"$OS_TYPE"
    echo 'Compiler:  '"$CC"
    [ "$CFLAGS_OS" != ' ' ] && echo 'C.Options: '"$CFLAGS_OS"
    [ "$LFLAGS_OS" != ' ' ] && echo 'L.Options: '"$LFLAGS_OS"
    echo ""
}
fi

find .. -path '*/c_extension/*.c' | \
while read -r extension
do
    is_hidden=`echo "$extension" | sed -n -e ':/_:p'`
    if [ -z "$is_hidden" ]
    then
        CompileExtension "$extension" $OS_LINUX   $CC_LIN "$CFLAGS_LIN" "$LFLAGS_LIN"
        CompileExtension "$extension" $OS_WINDOWS $CC_WIN "$CFLAGS_WIN" "$LFLAGS_WIN"
    fi
done
