#!/usr/bin/env python3

# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# ORIGINALLY in setup folder

import glob as gb_
import os as os_
import os.path as ph_
import shutil as sh_
import zipfile as zp_
from sys import platform as OS_TYPE
from os import pathsep as PATH_SEP
from typing import Sequence, Union

import pprint as pp_
import PyInstaller.__main__


PY_SCRIPT = "mpp_detector_cli.py"
BRICK_FOLDER = "brick"
HELPER_FOLDER = "helper"
RES_PATH_CMPS = ("resource", "twoD")
SRC_PATH_CMPS = ("..", "..")
README_PREFIX = "README"
RES_PREFIX = "microscopy"

DIST_ROOT_FOLDER = "_dist"
BUILD_ROOT_FOLDER = "_build"
PER_OS_DIST_FOLDER = f"Obj-MPP-4-{OS_TYPE.upper()}"
DST_RES_FOLDER = "resource"


def InBaseFolder(path: Union[str, Sequence[str]]) -> str:
    #
    if isinstance(path, str):
        return ph_.realpath(ph_.join(*SRC_PATH_CMPS, path))
    else:
        return ph_.realpath(ph_.join(*SRC_PATH_CMPS, *path))


SCRIPT_NAME = ph_.splitext(PY_SCRIPT)[0]
PER_OS_DIST_FOLDER_PATH = ph_.join(DIST_ROOT_FOLDER, PER_OS_DIST_FOLDER)
PER_OS_and_SCRIPT_DIST_PATH = ph_.join(PER_OS_DIST_FOLDER_PATH, SCRIPT_NAME)
PER_OS_BUILD_FOLDER_PATH = ph_.join(BUILD_ROOT_FOLDER, OS_TYPE)

PY_SCRIPT_PATH = InBaseFolder(PY_SCRIPT)
BRICK_FOLDER_PATH = InBaseFolder(BRICK_FOLDER)
HELPER_FOLDER_PATH = InBaseFolder(HELPER_FOLDER)
RES_FOLDER_PATH = InBaseFolder(ph_.join(*RES_PATH_CMPS))

DATA_ELEMENTS = (
    (BRICK_FOLDER_PATH, BRICK_FOLDER),
    (HELPER_FOLDER_PATH, HELPER_FOLDER),
)
BINARY_ELEMENTS = ()

arguments = [
    f"--distpath={PER_OS_DIST_FOLDER_PATH}",
    f"--specpath={PER_OS_BUILD_FOLDER_PATH}",
    f"--workpath={PER_OS_BUILD_FOLDER_PATH}",
    "--noconfirm",
    "--onedir",
    PY_SCRIPT_PATH,
]
for elm_type, elm_list in zip(
    ("--add-data", "--add-binary"), (DATA_ELEMENTS, BINARY_ELEMENTS)
):
    for element in elm_list:
        arguments.insert(
            arguments.__len__() - 1, f"{elm_type}={element[0]}{PATH_SEP}{element[1]}"
        )

for resource in gb_.glob(ph_.join(RES_FOLDER_PATH, f"{RES_PREFIX}*.png")):
    arguments.insert(
        arguments.__len__() - 1, f"--add-data={resource}{PATH_SEP}{DST_RES_FOLDER}"
    )
for resource in gb_.glob(ph_.join(RES_FOLDER_PATH, f"{RES_PREFIX}-*.ini")):
    arguments.insert(
        arguments.__len__() - 1, f"--add-data={resource}{PATH_SEP}{DST_RES_FOLDER}"
    )

arguments.insert(arguments.__len__() - 1, "--hidden-import=skimage")

for readme in gb_.glob(InBaseFolder(f"{README_PREFIX}*")):
    readme = ph_.realpath(readme)
    arguments.insert(arguments.__len__() - 1, f"--add-data={readme}{PATH_SEP}.")

if OS_TYPE == "win32":
    arguments.insert(arguments.__len__() - 1, "--console")

print("--- Launching PyInstaller with arguments:")
pp_.pprint(arguments)
PyInstaller.__main__.run(arguments)

print("--- Moving/copying elements into place")
if OS_TYPE == "win32":
    sh_.copy(f"{SCRIPT_NAME}.bat", PER_OS_DIST_FOLDER_PATH)
else:
    print(f"    /!\\ TODO: MAKE a SHELL SCRIPT for {OS_TYPE.upper()} and COPY IT")
sh_.move(ph_.join(PER_OS_and_SCRIPT_DIST_PATH, DST_RES_FOLDER), PER_OS_DIST_FOLDER_PATH)
for readme in gb_.glob(ph_.join(PER_OS_and_SCRIPT_DIST_PATH, f"{README_PREFIX}*")):
    sh_.move(readme, PER_OS_DIST_FOLDER_PATH)

print("--- Creating ZIP archive")
with zp_.ZipFile(
    ph_.join(DIST_ROOT_FOLDER, f"{PER_OS_DIST_FOLDER}.zip"),
    mode="w",
    compression=zp_.ZIP_DEFLATED,
    compresslevel=9,
) as archive:
    for folder, _, documents in os_.walk(PER_OS_DIST_FOLDER_PATH):
        for document in documents:
            doc_path = ph_.join(folder, document)
            archive.write(
                doc_path, arcname=ph_.relpath(doc_path, start=DIST_ROOT_FOLDER)
            )
