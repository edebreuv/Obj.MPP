# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Drawing inspired from: https://www.geeksforgeeks.org/pyqt5-create-paint-application/

from __future__ import annotations
import sys as sstm
from typing import Callable

from PyQt5.QtCore import QPoint, Qt
from PyQt5.QtGui import QImage, QPainter, QPen, QMouseEvent, QPaintEvent
from PyQt5.QtWidgets import QAction, QApplication, QMainWindow


class mpp_detector_t(QMainWindow):

    drawing_area: QImage = None
    drawing_mode: bool = False
    drawing_tip: QPoint = None
    pen: QPen = None

    def __init__(self) -> None:
        """"""
        super().__init__()

        self.setWindowTitle("Obj.MPP Detector")
        self.setGeometry(100, 100, 800, 600)

        self.drawing_area = QImage(self.size(), QImage.Format_RGB32)
        self.drawing_area.fill(Qt.white)
        self.pen = QPen(
            Qt.black,
            2,
            Qt.SolidLine,
            Qt.RoundCap,
            Qt.RoundJoin,
        )

        menu_bar = self.menuBar()
        main_menu = menu_bar.addMenu("Obj.MPP")
        size_menu = menu_bar.addMenu("Brush Size")
        color_menu = menu_bar.addMenu("Brush Color")

        action = QAction("Clear", self)
        main_menu.addAction(action)
        action.triggered.connect(self.Clear)

        action = QAction("Quit", self)
        main_menu.addAction(action)
        action.triggered.connect(self.close)

        for size in (2, 4, 7, 9, 12):
            action = QAction(f"{size}px", self)
            size_menu.addAction(action)
            action.triggered.connect(self.FunctionToSetBrushSize(size))

        for color in ("Black", "White", "Red", "Green", "Blue", "Yellow"):
            action = QAction(color, self)
            color_menu.addAction(action)
            action.triggered.connect(self.FunctionToSetBrushColor(color))

    def mousePressEvent(self, event: QMouseEvent, /) -> None:
        """"""
        if event.button() == Qt.LeftButton:
            self.drawing_mode = True
            self.drawing_tip = event.pos()

    def mouseMoveEvent(self, event: QMouseEvent, /) -> None:
        """"""
        if self.drawing_mode and (event.buttons() & Qt.LeftButton):
            painter = QPainter(self.drawing_area)
            painter.setPen(self.pen)
            painter.drawLine(self.drawing_tip, event.pos())

            self.drawing_tip = event.pos()

            self.update()

    def mouseReleaseEvent(self, event: QMouseEvent, /) -> None:
        """"""
        if event.button() == Qt.LeftButton:
            self.drawing_mode = False

    def paintEvent(self, event: QPaintEvent, /) -> None:
        """"""
        QPainter(self).drawImage(
            self.rect(), self.drawing_area, self.drawing_area.rect()
        )

    def Clear(self) -> None:
        """"""
        self.drawing_area.fill(Qt.white)

        self.update()

    def FunctionToSetBrushSize(self, size: int, /) -> Callable[[], None]:
        """"""

        def _SetBrushSize() -> None:
            """"""
            self.pen.setWidth(size)

        return _SetBrushSize

    def FunctionToSetBrushColor(self, color: str, /) -> Callable[[], None]:
        """"""

        def _SetBrushColor() -> None:
            """"""
            self.pen.setColor(getattr(Qt, color.lower()))

        return _SetBrushColor


app = QApplication(sstm.argv)
window = mpp_detector_t()
window.show()
sstm.exit(app.exec())
