//Copyright CNRS/Inria/UNS
//Contributor(s): Eric Debreuve (since 2018)
//
//eric.debreuve@cnrs.fr
//
//This software is governed by the CeCILL  license under French law and
//abiding by the rules of distribution of free software.  You can  use,
//modify and/ or redistribute the software under the terms of the CeCILL
//license as circulated by CEA, CNRS and INRIA at the following URL
//"http://www.cecill.info".
//
//As a counterpart to the access to the source code and  rights to copy,
//modify and redistribute granted by the license, users are provided only
//with a limited warranty  and the software's author,  the holder of the
//economic rights,  and the successive licensors  have only  limited
//liability.
//
//In this respect, the user's attention is drawn to the risks associated
//with loading,  using,  modifying and/or developing or reproducing the
//software by the user in light of its specific status of free software,
//that may mean  that it is complicated to manipulate,  and  that  also
//therefore means  that it is reserved for developers  and  experienced
//professionals having in-depth computer knowledge. Users are therefore
//encouraged to load and test the software's suitability as regards their
//requirements in conditions enabling the security of their systems and/or
//data to be ensured and,  more generally, to use and operate it in the
//same conditions as regards security.
//
//The fact that you are presently reading this means that you have had
//knowledge of the CeCILL license and that you accept its terms.

// Code below is valid only if type double matches numpy type float64

#include <math.h>  // cos, fabs, pow, sin
//#include <omp.h>
#include <stdio.h> // size_t



void
Region(
    double* bbox_grid_rows, double* bbox_grid_cols, size_t n_elements,
    double center_row, double center_col,
    double semi_minor_axis, double major_minor_ratio,
    double angle_radian,
    unsigned char* result) {
    //
    double semi_major_axis = semi_minor_axis * major_minor_ratio;
    double minor_factor = 1.0 / (semi_minor_axis * semi_minor_axis);
    double major_factor = 1.0 / (semi_major_axis * semi_major_axis);
    double cosine = cos(angle_radian);
    double sine   = sin(angle_radian);

    /*
    #pragma omp parallel for \
        default(none) \
        shared(bbox_grid_rows, bbox_grid_cols, n_elements, \
               center_row, center_col, \
               sq_distance_map, \
               minor_factor, major_factor, cosine, sine)
    */
    for (size_t idx = 0; idx < n_elements; idx++) {
        double centered_row = bbox_grid_rows[idx] - center_row;
        double centered_col = bbox_grid_cols[idx] - center_col;

        double rotated_row = sine   * centered_col + cosine * centered_row;
        double rotated_col = cosine * centered_col - sine   * centered_row;

        result[idx] = ((rotated_row * rotated_row) * minor_factor
                    +  (rotated_col * rotated_col) * major_factor) <= 1.0;
    }
}
