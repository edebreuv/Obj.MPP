# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Run with: PYTHONPATH=. python resource/threeD/create-ellipsoids.py

n_rows = 50
n_cols = n_rows
n_deps = n_rows

n_ellipsoids = 8
seed = 0

min_minor_axis = 3
max_minor_axis = 6

img_name = "ellipsoids.tif"
gt_name = "ellipsoids.txt"

# --- EDITION BARRIER ---

sm_ny_name = img_name[:-4] + "-smooth-noisy.tif"

from brick.signal.signal_context import signal_context_t
from brick.marked_point.threeD.ellipsoid import ellipsoid_t

import os.path as ph_  # Pathlib not necessary

import numpy as nmpy


if ph_.exists(img_name) or ph_.exists(sm_ny_name) or ph_.exists(gt_name):
    print(f"{img_name}, {sm_ny_name}, and/or {gt_name}: already exist(s)")
    answer = input("Overwrite [y=yes, anything else=no]? ")

    if answer != "y":
        import sys as sy_

        sy_.exit(0)


with open(gt_name, "w") as gt_accessor:
    vol_shape = (n_rows, n_cols, n_deps)
    image = nmpy.zeros(vol_shape, dtype=nmpy.uint8, order="C")
    signal_context_t.SetSignalsForQualityAndStatistics(vol_shape, None, None)

    number_sampler = nmpy.random.default_rng(seed=seed)

    centers = number_sampler.integers(
        0, high=n_rows, size=(n_ellipsoids, 3), dtype=nmpy.uint8
    )
    minor_axes = number_sampler.integers(
        min_minor_axis, high=max_minor_axis + 1, size=n_ellipsoids, dtype=nmpy.uint8
    )
    major_ratios = number_sampler.uniform(low=1.0, high=1.5, size=n_ellipsoids)
    third_ratios = number_sampler.uniform(low=0.6, high=1.5, size=n_ellipsoids)
    rc_angles = number_sampler.uniform(low=0.0, high=nmpy.pi, size=n_ellipsoids)
    rd_angles = number_sampler.uniform(low=0.0, high=2 * nmpy.pi, size=n_ellipsoids)

    actual_n_ellipsoids = 0
    for idx in range(n_ellipsoids):
        ellipsoid = ellipsoid_t(
            centers[idx, :],
            minor_axes[idx],
            major_ratios[idx],
            third_ratios[idx],
            rc_angles[idx],
            rd_angles[idx],
            check_marks=True,
        )
        local_image = image[ellipsoid.bbox.domain]
        region = ellipsoid.region
        if nmpy.all(local_image[region] == 0) and not ellipsoid.crosses_border:
            local_image[region] = 255
            print(
                centers[idx, :],
                minor_axes[idx],
                nmpy.round(minor_axes[idx] * major_ratios[idx], 2),
                nmpy.round(minor_axes[idx] * third_ratios[idx], 2),
                nmpy.round(180.0 * rc_angles[idx] / nmpy.pi),
                nmpy.round(180.0 * rd_angles[idx] / nmpy.pi),
                file=gt_accessor,
            )
            actual_n_ellipsoids += 1

    print(f"Actual number of ellipsoids: {actual_n_ellipsoids}")

    import scipy.ndimage as im_

    img_sm_ny = im_.gaussian_filter(image, 1.5, mode="wrap") + number_sampler.normal(
        scale=25, size=image.shape
    )
    img_sm_ny[img_sm_ny < 0.0] = 0.0
    img_sm_ny[img_sm_ny > 255.0] = 255.0
    img_sm_ny = nmpy.rint(img_sm_ny).astype(nmpy.uint8)

    import imageio as io_

    io_.volwrite(img_name, image)
    io_.volwrite(sm_ny_name, img_sm_ny)
