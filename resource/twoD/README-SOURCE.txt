Annotation:
    convert microscopy-unannotated.png \
            -pointsize 10 -font Source-Code-Pro \
            -annotate -90x-90+12+540 "`cat README-SOURCE.txt | tail -n 4`" \
            microscopy.png

Image:    microscopy.png
Original: К562_(Light_microscopy).JPG
From:     https://commons.wikimedia.org/wiki/File:%D0%9A562_(Light_microscopy).JPG
License:  Creative Commons Attribution-Share Alike 4.0 International
          (https://creativecommons.org/licenses/by-sa/4.0/deed.en)
